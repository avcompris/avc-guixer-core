package net.avcompris.guixer.core;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;

import org.junit.jupiter.api.Test;

import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.guixer.core.utils.Run;

public class DumperLoggerTest {

	@Test
	public void testDumperLogger() throws Exception {

		final File destDir = new File("target");

		final Context context = new Context(destDir);

		context.setSeleniumServerURL("http://dummy:4444/wd/hub");
		context.setSeleniumDesiredCapabilities("firefox");
		context.setBaseURL("http://dummy");

		final DumperLogger dumperLogger = DumperLogger.getLogger(context);

		dumperLogger.close();

		final File logFile = new File(destDir, DumperLoggerTest.class.getSimpleName() //
				+ "/testDumperLogger/undefined/log.xml");

		final Run log = DomBinderUtils.xmlContentToJava(logFile, Run.class);

		final String guixerVersion = log.getGuixerVersion();

		assertNotNull(guixerVersion);

		assertNotEquals("", guixerVersion.trim());

		assertFalse(guixerVersion.contains("${"), "guixerVersion should not contain \"${\", but was: " + guixerVersion);
	}
}
