package net.avcompris.guixer.core;

import static net.avcompris.guixer.core.LocatorUtils.by;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

public class LocatorUtilsTest {

	@Test
	public void test_by_id() {

		assertEquals(By.id("toto"), by("#toto"));
	}

	@Test
	public void test_by_xpath() {

		assertEquals(By.xpath("//toto[@src = 'x']"), by("//toto[@src = 'x']"));
	}
}
