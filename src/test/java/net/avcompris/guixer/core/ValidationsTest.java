package net.avcompris.guixer.core;

import static net.avcompris.guixer.core.Validations.assertValidLabel;
import static net.avcompris.guixer.core.Validations.assertValidTestClassSimpleName;
import static net.avcompris.guixer.core.Validations.assertValidTestMethodName;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class ValidationsTest {

	@Test
	public void testValidMethodNames() {

		assertValidTestMethodName("testCoco");
		assertValidTestMethodName("test");
		assertValidTestMethodName("coco");
		assertValidTestMethodName("coco_xxx");
		assertValidTestMethodName("coco123");
	}

	@Test
	public void testInvalidMethodName_parenthesis() {

		assertThrows(AssertionError.class, ()

		-> assertValidTestMethodName("testCoco()"));
	}

	@Test
	public void testInvalidMethodName_UppercaseLetter() {

		assertThrows(AssertionError.class, ()

		-> assertValidTestMethodName("UppercaseLetter"));
	}

	@Test
	public void testInvalidMethodName_accent() {

		assertThrows(AssertionError.class, ()

		-> assertValidTestMethodName("trèsBien"));
	}

	@Test
	public void testInvalidMethodName_dash() {

		assertThrows(AssertionError.class, ()

		-> assertValidTestMethodName("xx-zz"));
	}

	@Test
	public void testValidClassSimpleNames() {

		assertValidTestClassSimpleName("CocoTest");
		assertValidTestClassSimpleName("Coco_Test");
		assertValidTestClassSimpleName("Coco123Test");
	}

	@Test
	public void testInvalidClassSimpleName_dollar() {

		assertThrows(AssertionError.class, ()

		-> assertValidTestClassSimpleName("Coco$Test"));
	}

	@Test
	public void testInvalidClassSimpleName_LowercaseLetter() {

		assertThrows(AssertionError.class, ()

		-> assertValidTestClassSimpleName("lowercaseLetterTest"));
	}

	@Test
	public void testInvalidClassSimpleName_accent() {

		assertThrows(AssertionError.class, ()

		-> assertValidTestClassSimpleName("TrèsBienTest"));
	}

	@Test
	public void testInvalidClassSimpleName_dash() {

		assertThrows(AssertionError.class, ()

		-> assertValidTestClassSimpleName("Xx-zzTest"));
	}

	@Test
	public void testValidLabel() {

		assertValidLabel("CocoTest");
		assertValidLabel("Coco_Test");
		assertValidLabel("coco");
		assertValidLabel("123coco");
		assertValidLabel("coco-xx");
		assertValidLabel("a.b.c");
	}

	@Test
	public void testInvalidLabel_dollar() {

		assertThrows(AssertionError.class, ()

		-> assertValidLabel("Coco$Test"));
	}

	@Test
	public void testInvalidLabel_space() {

		assertThrows(AssertionError.class, ()

		-> assertValidLabel("my label"));
	}

	@Test
	public void testInvalidLabel_accent() {

		assertThrows(AssertionError.class, ()

		-> assertValidLabel("TrèsBien"));
	}

	@Test
	public void testInvalidLabel_comma() {

		assertThrows(AssertionError.class, ()

		-> assertValidLabel("x,y,z"));
	}

	@Test
	public void testInvalidLabel_slash() {

		assertThrows(AssertionError.class, ()

		-> assertValidLabel("x/z"));
	}

	@Test
	public void testInvalidLabel_trailingDot() {

		assertThrows(AssertionError.class, ()

		-> assertValidLabel("x."));
	}

	@Test
	public void testInvalidLabel_leadingDot() {

		assertThrows(AssertionError.class, ()

		-> assertValidLabel(".x"));
	}
}
