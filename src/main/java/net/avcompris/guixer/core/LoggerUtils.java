package net.avcompris.guixer.core;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.openqa.selenium.Keys;

abstract class LoggerUtils {

	public static String escape(@Nullable final String text) {

		if (text == null) {

			return "null";

		} else {

			return "\"" //
					+ text //
							.replace("\\", "\\\\") //
							.replace("\"", "\\\"") //
					+ "\"";
		}
	}

	public static String formatKeys(final CharSequence[] keys) {

		final StringBuilder sb = new StringBuilder();

		for (final CharSequence cs : keys) {

			sb.append(", ");

			if (cs == null) {

				sb.append("null");

			} else if (cs instanceof String) {

				sb.append(escape((String) cs));

			} else if (cs instanceof Keys) {

				sb.append(Keys.class.getSimpleName() + ((Keys) cs).name());

			} else {

				throw new NotImplementedException("cs.class: " + cs.getClass().getName() + " (" + cs + ")");
			}
		}

		return sb.toString();
	}
}
