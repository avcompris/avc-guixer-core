package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import javax.annotation.Nullable;

final class Assertion {

	private final String assertionName;
	private final String arg0;
	private final String arg1;

	@Nullable
	private Throwable exception;

	private boolean success = false;

	public Assertion(final String assertionName, final String arg0, final String arg1) {

		this.assertionName = checkNotNull(assertionName, "assertionName");
		this.arg0 = checkNotNull(arg0, "arg0");
		this.arg1 = checkNotNull(arg1, "arg1");
	}

	@Override
	public String toString() {

		final StringBuilder sb = new StringBuilder(assertionName + "(\"" + arg0 + "\", \"" + arg1 + "\"): ");

		if (exception != null) {

			sb.append("ERROR: " + exception.getClass().getSimpleName());

		} else if (success) {

			sb.append("SUCCESS");

		} else {

			sb.append("PENDING");
		}

		return sb.toString();
	}

	public void setException(@Nullable final Throwable e) {

		checkState(!success || e == null, //
				"exception should not be non null when success == true: %s", e);

		checkState(exception == null || e == null, //
				"exception should not be non null when a previous exception exists: %s (%s)", e, exception);

		checkState(exception == null || e != null,
				"exception should not be set to null when a previous exception exists: %s", exception);

		exception = e;
	}

	public void setSuccess() {

		checkState(exception == null, //
				"success should not be set when a previous exception exists: %s", exception);

		success = true;
	}

	public boolean isFailure() {

		return exception != null;
	}

	public boolean isPending() {

		return !success && exception == null;
	}

	public boolean isSuccess() {

		return success;
	}
}
