package net.avcompris.guixer.core;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.Nullable;

interface Logger {

	void setTestContext(Class<?> testClass, Method testMethod) throws IOException;

	void close() throws IOException;

	void startCommand(@Nullable String actionShortDescription) throws IOException;

	void startStep(String label) throws IOException;

	void error(Throwable throwable) throws IOException;

	void endStep() throws IOException;
}
