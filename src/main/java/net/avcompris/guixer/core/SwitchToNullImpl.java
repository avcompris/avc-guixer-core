package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

final class SwitchToNullImpl implements SwitchTo {

	private final Command underlyingCommand;

	public SwitchToNullImpl(final Command underlyingCommand) {

		this.underlyingCommand = checkNotNull(underlyingCommand, "underlyingCommand");
	}

	@Override
	public Command defaultContent() throws IOException {

		// do nothing

		return underlyingCommand;
	}

	@Override
	public Command parentFrame() throws IOException {

		// do nothing

		return underlyingCommand;
	}

	@Override
	public Command frame(final String name) throws IOException {

		// do nothing

		return underlyingCommand;
	}
}
