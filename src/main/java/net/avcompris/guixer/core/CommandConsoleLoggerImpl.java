package net.avcompris.guixer.core;

import java.io.IOException;

import javax.annotation.Nullable;

import org.openqa.selenium.WebDriverException;

final class CommandConsoleLoggerImpl extends AbstractCommandLoggerImpl {

	public CommandConsoleLoggerImpl( //
			final Command delegate, //
			final Context context, //
			@Nullable final String actionShortDescription //
	) throws IOException {

		super(delegate, context, context.getConsoleLogger(), actionShortDescription);
	}

	public CommandConsoleLoggerImpl( //
			final Command delegate, //
			final Context context //
	) throws IOException {

		this(delegate, context, null);
	}

	@Override
	protected void handleError(final WebDriverException e) throws IOException {

		logger.error(e);

		throw e;
	}
}
