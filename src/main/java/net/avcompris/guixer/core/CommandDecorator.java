package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;

abstract class CommandDecorator implements Command {

	protected final Command delegate;

	protected CommandDecorator(final Command delegate) {

		this.delegate = checkNotNull(delegate, "delegate");
	}
}
