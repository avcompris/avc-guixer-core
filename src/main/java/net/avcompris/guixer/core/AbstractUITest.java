package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static net.avcompris.commons3.it.utils.IntegrationTestUtils.getTestProperty;
import static org.apache.commons.lang3.StringUtils.substringBetween;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URL;

import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class AbstractUITest extends MyAbstractUITestCommandImpl {

	private WebDriver driver;
	private String baseURL;
	private static final Context context = new Context(new File("target/ui-it-results"));

	protected AbstractUITest() {

		super(context);
	}

	protected final WebDriver getDriver() {

		final WebDriver driverCopy = driver;

		checkState(driverCopy != null, "driver should have been set");

		return driverCopy;
	}

	protected final String getBaseURL() {

		final String baseURLCopy = baseURL;

		checkState(baseURLCopy != null, "baseURL should have been set");

		return baseURLCopy;
	}

	@BeforeEach
	public final void setUpFirefox() throws Exception {

		final String seleniumServerURL = getTestProperty("selenium.server.url");
		final String seleniumDesiredCapabilities = getTestProperty("selenium.desiredCapabilities");

		System.out.println("selenium.server.url: " + seleniumServerURL);
		System.out.println("selenium.desiredCapabilities: " + seleniumDesiredCapabilities);

		context.setSeleniumServerURL(seleniumServerURL);
		context.setSeleniumDesiredCapabilities(seleniumDesiredCapabilities);

		final Capabilities capabilities;

		if ("firefox".contentEquals(seleniumDesiredCapabilities)) {
			capabilities = DesiredCapabilities.firefox();
		} else if ("chrome".contentEquals(seleniumDesiredCapabilities)) {
			capabilities = DesiredCapabilities.chrome();
		} else {
			throw new NotImplementedException("selenium.desiredCapabilities: " + seleniumDesiredCapabilities);
		}

		driver = new RemoteWebDriver(new URL(seleniumServerURL), capabilities);
	}

	@AfterEach
	public final void tearDownFirefox() throws Exception {

		if (driver != null) {

			try {

				driver.quit();

			} catch (final WebDriverException e) {

				// Sometimes, "driver.quit()" just crashes.
				//
				e.printStackTrace(); // log the error, and do nothing
			}

			driver = null;
		}
	}

	@BeforeEach
	public final void setUpContext() throws Exception {

		context.reset(Long.toString(System.currentTimeMillis()));

		final String oldBaseURL = baseURL;

		final Class<? extends AbstractUITest> testClass = this.getClass();

		String baseURL = extractAnnotation(testClass, UIEnv.class).baseURL();

		if (baseURL.startsWith("${")) {

			baseURL = getTestProperty(substringBetween(baseURL, "${", "}"));
		}

		if (oldBaseURL == null || !baseURL.contentEquals(oldBaseURL)) {

			System.out.println("baseURL: " + baseURL);
		}

		this.baseURL = baseURL;

		context.setBaseURL(baseURL);
	}

	@AfterEach
	public final void tearDownContext() throws Exception {

		context.close();
	}

	private <T extends Annotation> T extractAnnotation(final Class<?> clazz, final Class<T> annotationClass) {

		checkNotNull(clazz, "clazz");
		checkNotNull(annotationClass, "annotationClass");

		final T annotation = clazz.getAnnotation(annotationClass);

		if (annotation != null) {
			return annotation;
		}

		final Class<?> superClass = clazz.getSuperclass();

		if (superClass == null) {
			throw new RuntimeException("Cannot find annotation: @" + annotationClass.getSimpleName() + ", on class: "
					+ this.getClass().getName());
		}

		return extractAnnotation(superClass, annotationClass);
	}
}
