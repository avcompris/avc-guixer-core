package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

final class SwitchToLoggerImpl extends SwitchToDecorator implements SwitchTo {

	private final AbstractCommandLoggerImpl underlyingCommand;
	private final Logger logger;

	public SwitchToLoggerImpl(final SwitchTo delegate, final AbstractCommandLoggerImpl underlyingCommand, final Logger logger)
			throws IOException {

		super(delegate);

		this.underlyingCommand = checkNotNull(underlyingCommand, "underlyingCommand");
		this.logger = checkNotNull(logger, "logger");
	}

	@Override
	public Command defaultContent() throws IOException {

		logger.startStep("switchTo().defaultContent()");

		delegate.defaultContent();

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command parentFrame() throws IOException {

		logger.startStep("switchTo().parentFrame()");

		delegate.parentFrame();

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command frame(final String name) throws IOException {

		logger.startStep("switchTo().frame(\"" + name + "\")");

		delegate.frame(name);

		logger.endStep();

		return underlyingCommand;
	}
}
