package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;

abstract class SwitchToDecorator implements SwitchTo {

	protected final SwitchTo delegate;

	protected SwitchToDecorator(final SwitchTo delegate) {

		this.delegate = checkNotNull(delegate, "delegate");
	}
}
