package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

class MyAbstractUITestCommandImpl implements Command {

	private final Context context;

	MyAbstractUITestCommandImpl(final Context context) {

		this.context = checkNotNull(context, "context");
	}

	protected WebDriver getDriver() {

		throw new IllegalStateException("MyAbstractUITestCommandImpl shoud be subclassed, and getDriver() overriden");
	}

	protected String getBaseURL() {

		throw new IllegalStateException("MyAbstractUITestCommandImpl shoud be subclassed, and getBaseURL() overriden");
	}

	protected final Command command() throws IOException {

		return new CommandConsoleLoggerImpl( //
				new CommandDumperLoggerImpl( //
						new CommandSeleniumImpl( //
								getDriver(), //
								getBaseURL(), //
								context), //
						context), //
				context);
	}

	protected final Command command(final String actionShortDescription) throws IOException {

		checkNotNull(actionShortDescription, "actionShortDescription");

		return new CommandConsoleLoggerImpl( //
				new CommandDumperLoggerImpl( //
						new CommandSeleniumImpl( //
								getDriver(), //
								getBaseURL(), //
								context), //
						context, //
						actionShortDescription), //
				context, //
				actionShortDescription);
	}

	@Override
	public final Command waitFor(final String locator) throws IOException {

		return command().waitFor(locator);
	}

	@Override
	public final Command waitFor(final String locator, final int timeOutSeconds) throws IOException {

		return command().waitFor(locator, timeOutSeconds);
	}

	@Override
	public final Command waitForVisible(final String locator) throws IOException {

		return command().waitForVisible(locator);
	}

	@Override
	public final Command waitForVisible(final String locator, final int timeOutSeconds) throws IOException {

		return command().waitForVisible(locator, timeOutSeconds);
	}

	@Override
	public final Command sleep(final int ms) throws IOException {

		return command().sleep(ms);
	}

	@Override
	public final Command takeScreenshot() throws IOException {

		return command().takeScreenshot();
	}

	@Override
	public final Command takeScreenshot(final String label) throws IOException {

		return command().takeScreenshot(label);
	}

	@Override
	public Command get() throws IOException {

		return command().get();
	}

	@Override
	public SwitchTo switchTo() throws IOException {

		return command().switchTo();
	}

	@Override
	public Command log(final String text) throws IOException {

		return command().log(text);
	}

	@Override
	public Command clear(final String locator) throws IOException {

		return command().clear(locator);
	}

	@Override
	public Command click(final String locator) throws IOException {

		return command().click(locator);
	}

	@Override
	public Command submit(final String locator) throws IOException {

		return command().submit(locator);
	}

	@Override
	public Command select(final String locator, final int value) throws IOException {

		return command().select(locator, value);
	}

	@Override
	public Command select(final String locator, final String value) throws IOException {

		return command().select(locator, value);
	}

	@Override
	public Command assertHasClass(final String locator, final String className) throws IOException {

		return command().assertHasClass(locator, className);
	}

	@Override
	public Command assertDoesntHaveClass(final String locator, final String className) throws IOException {

		return command().assertDoesntHaveClass(locator, className);
	}

	@Override
	public Command sendKeys(final String locator, final CharSequence... keys) throws IOException {

		return command().sendKeys(locator, keys);
	}

	@Override
	public Command sendKeysSecret(final String locator, final String secret) throws IOException {

		return command().sendKeysSecret(locator, secret);
	}

	@Override
	public Command skip() throws IOException {

		return command().skip();
	}

	@Override
	public Command when(final String label, final boolean condition) throws IOException {

		return command().when(label, condition);
	}

	@Override
	public Command when(final boolean condition) throws IOException {

		return command().when(condition);
	}
}
