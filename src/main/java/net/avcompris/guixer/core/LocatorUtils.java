package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.commons.lang3.NotImplementedException;
import org.openqa.selenium.By;

abstract class LocatorUtils {

	public static By by(final String locator) {

		checkNotNull(locator, "locator");

		if (locator.startsWith("#")) {

			return By.id(locator.substring(1));

		} else if (locator.startsWith("//")) {

			return By.xpath(locator);

		} else if (locator.startsWith("css:")) {

			return By.cssSelector(locator.substring(4));

		} else {

			throw new NotImplementedException("locator: " + locator);
		}
	}
}
