package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;

import org.openqa.selenium.WebDriver.TargetLocator;

final class SwitchToSeleniumImpl implements SwitchTo {

	private final TargetLocator targetLocator;
	private final CommandSeleniumImpl underlyingCommand;

	public SwitchToSeleniumImpl(final TargetLocator targetLocator, final CommandSeleniumImpl underlyingCommand) {

		this.targetLocator = checkNotNull(targetLocator, "targetLocator");
		this.underlyingCommand = checkNotNull(underlyingCommand, "underlyingCommand");
	}

	@Override
	public Command defaultContent() {

		targetLocator.defaultContent();

		return underlyingCommand;
	}

	@Override
	public Command parentFrame() {

		targetLocator.parentFrame();

		return underlyingCommand;
	}

	@Override
	public Command frame(final String name) {

		targetLocator.frame(name);

		return underlyingCommand;
	}
}
