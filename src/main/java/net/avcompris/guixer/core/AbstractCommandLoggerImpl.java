package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.guixer.core.LoggerUtils.escape;
import static net.avcompris.guixer.core.LoggerUtils.formatKeys;

import java.io.IOException;

import javax.annotation.Nullable;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;

abstract class AbstractCommandLoggerImpl extends CommandDecorator implements Command {

	@Nullable
	private final String actionShortDescription;

	protected final Logger logger;

	protected AbstractCommandLoggerImpl( //
			final Command delegate, //
			final Context context, //
			final Logger logger, //
			@Nullable final String actionShortDescription //
	) throws IOException {

		super(delegate);

		this.actionShortDescription = actionShortDescription;

		this.logger = checkNotNull(logger, "logger");

		// context.getDumperLogger();

		logger.startCommand(actionShortDescription);
	}

	protected AbstractCommandLoggerImpl( //
			final Command delegate, //
			final Context context, //
			final Logger logger //
	) throws IOException {

		this(delegate, context, logger, null);
	}

	@Override
	public final Command get() throws IOException {

		logger.startStep("get()");

		delegate.get();

		logger.endStep();

		return this;
	}

	@Override
	public final Command takeScreenshot() throws IOException {

		logger.startStep("takeScreenshot()");

		delegate.takeScreenshot();

		logger.endStep();

		return this;
	}

	@Override
	public final Command takeScreenshot(final String label) throws IOException {

		logger.startStep("takeScreenshot(" + escape(label) + ")");

		delegate.takeScreenshot(label);

		logger.endStep();

		return this;
	}

	@Override
	public final Command sleep(final int ms) throws IOException {

		logger.startStep("sleep(" + ms + ")");

		delegate.sleep(ms);

		logger.endStep();

		return this;
	}

	@Override
	public final Command waitFor(final String locator) throws IOException {

		logger.startStep("waitFor(" + escape(locator) + ")");

		try {

			delegate.waitFor(locator);

		} catch (final TimeoutException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command waitFor(final String locator, final int timeOutSeconds) throws IOException {

		logger.startStep("waitFor(" + escape(locator) + ", " + timeOutSeconds + ")");

		try {

			delegate.waitFor(locator, timeOutSeconds);

		} catch (final TimeoutException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command waitForVisible(final String locator) throws IOException {

		logger.startStep("waitForVisible(" + escape(locator) + ")");

		try {

			delegate.waitForVisible(locator);

		} catch (final TimeoutException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command waitForVisible(final String locator, final int timeOutSeconds) throws IOException {

		logger.startStep("waitForVisible(" + escape(locator) + ", " + timeOutSeconds + ")");

		try {

			delegate.waitForVisible(locator, timeOutSeconds);

		} catch (final TimeoutException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final SwitchTo switchTo() throws IOException {

		return new SwitchToLoggerImpl( //
				delegate.switchTo(), //
				this, //
				logger);
	}

	@Override
	public final Command log(@Nullable final String text) throws IOException {

		logger.startStep("log(" + escape(text) + ")");

		delegate.log(text);

		logger.endStep();

		return this;
	}

	@Override
	public final Command clear(final String locator) throws IOException {

		logger.startStep("clear(" + escape(locator) + ")");

		try {

			delegate.clear(locator);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command click(final String locator) throws IOException {

		logger.startStep("click(" + escape(locator) + ")");

		try {

			delegate.click(locator);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command submit(final String locator) throws IOException {

		logger.startStep("submit(" + escape(locator) + ")");

		try {

			delegate.submit(locator);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command select(final String locator, final int value) throws IOException {

		logger.startStep("select(" + escape(locator) + ", " + value + ")");

		try {

			delegate.select(locator, value);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command select(final String locator, final String value) throws IOException {

		logger.startStep("select(" + escape(locator) + ", " + escape(value) + ")");

		try {

			delegate.select(locator, value);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	protected abstract void handleError(WebDriverException e) throws IOException;

	@Override
	public final Command assertHasClass(final String locator, final String className) throws IOException {

		logger.startStep("assertHasClass(" + escape(locator) + ", " + escape(className) + ")");

		try {

			delegate.assertHasClass(locator, className);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command assertDoesntHaveClass(final String locator, final String className) throws IOException {

		logger.startStep("assertDoesntHaveClass(" + escape(locator) + ", " + escape(className) + ")");

		try {

			delegate.assertDoesntHaveClass(locator, className);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command sendKeys(final String locator, final CharSequence... keys) throws IOException {

		logger.startStep("sendKeys(" + escape(locator) + "" + formatKeys(keys) + ")");

		try {

			delegate.sendKeys(locator, keys);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command sendKeysSecret(final String locator, final String secret) throws IOException {

		logger.startStep("sendKeysSecret(" + escape(locator) + ", ***)");

		try {

			delegate.sendKeysSecret(locator, secret);

		} catch (final WebDriverException e) {

			handleError(e);
		}

		logger.endStep();

		return this;
	}

	@Override
	public final Command skip() throws IOException {

		final Command skip = new Skip(this, logger);

		return skip;
	}

	private final Command applyWhen(final boolean condition) throws IOException {

		return condition ? this : skip();
	}

	@Override
	public final Command when(final String label, boolean condition) throws IOException {

		logger.startStep("when(" + escape(label) + "," + condition + ")");

		final Command when = applyWhen(condition);

		logger.endStep();

		return when;
	}

	@Override
	public final Command when(final boolean condition) throws IOException {

		logger.startStep("when(" + condition + ")");

		final Command when = applyWhen(condition);

		logger.endStep();

		return when;
	}
}
