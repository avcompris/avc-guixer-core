package net.avcompris.guixer.core;

import java.io.IOException;

public interface SwitchTo {

	Command defaultContent() throws IOException;

	Command parentFrame() throws IOException;

	Command frame(String name) throws IOException;
}
