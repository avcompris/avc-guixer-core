package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.annotation.Nullable;

final class ConsoleLogger implements Logger {

	private static final Map<File, ConsoleLogger> LOGGERS = newHashMap();

	private final File dir;

	private ConsoleLogger(final File dir) {

		this.dir = checkNotNull(dir, "dir");
	}

	public static ConsoleLogger getLogger(final Context context) throws IOException {

		checkNotNull(context, "context");

		final File dir = context.getSubDir();

		if (!dir.isDirectory()) {

			throw new FileNotFoundException("dir should be a directory: " + dir.getCanonicalPath());
		}

		if (LOGGERS.containsKey(dir)) {

			return LOGGERS.get(dir);
		}

		final ConsoleLogger logger = new ConsoleLogger(dir);

		LOGGERS.put(dir, logger);

		return logger;
	}

	@Override
	public int hashCode() {

		return dir.hashCode();
	}

	@Override
	public boolean equals(@Nullable final Object arg) {

		if (arg == null || !(arg instanceof ConsoleLogger)) {

			return false;
		}

		return dir.equals(((ConsoleLogger) arg).dir);
	}

	@Override
	public String toString() {

		return "[" + dir.getAbsolutePath() + "]";
	}

	@Override
	public void setTestContext(final Class<?> testClass, final Method testMethod) {

		System.out.println("> test: " + testClass.getSimpleName() //
				+ "." + testMethod.getName() + "()");
	}

	@Override
	public void close() {

		// do nothing
	}

	@Override
	public void startCommand(@Nullable final String actionShortDescription) {

		if (actionShortDescription == null) {

			System.out.println("command()");

		} else {

			System.out.println("command(\"" + actionShortDescription + "\")");
		}
	}

	@Override
	public void startStep(final String label) {

		System.out.println("  ." + label);
	}

	@Override
	public void endStep() throws IOException {

		// do nothing
	}

	@Override
	public void error(final Throwable throwable) {

		System.err.println(throwable);
	}
}
