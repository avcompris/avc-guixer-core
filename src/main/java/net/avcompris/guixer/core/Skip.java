package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.guixer.core.LoggerUtils.escape;
import static net.avcompris.guixer.core.LoggerUtils.formatKeys;

import java.io.IOException;

import javax.annotation.Nullable;

final class Skip implements Command {

	private final AbstractCommandLoggerImpl underlyingCommand;
	private final Logger logger;

	public Skip( //
			final AbstractCommandLoggerImpl underlyingCommand, //
			final Logger logger) throws IOException {

		this.underlyingCommand = checkNotNull(underlyingCommand, "underlyingCommand");
		this.logger = checkNotNull(logger, "logger");
	}

	@Override
	public Command get() throws IOException {

		logger.startStep("skip(" + escape("get()") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command takeScreenshot() throws IOException {

		logger.startStep("skip(" + escape("takeScreenshot()") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command takeScreenshot(final String label) throws IOException {

		logger.startStep("skip(" + escape("takeScreenshot(" + escape(label) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command sleep(final int ms) throws IOException {

		logger.startStep("skip(" + escape("sleep(" + ms + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command waitFor(final String locator) throws IOException {

		logger.startStep("skip(" + escape("waitFor(" + escape(locator) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command waitFor(final String locator, final int timeOutSeconds) throws IOException {

		logger.startStep("skip(" + escape("waitFor(" + escape(locator) + ", " + timeOutSeconds + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command waitForVisible(final String locator) throws IOException {

		logger.startStep("skip(" + escape("waitForVisible(" + escape(locator) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command waitForVisible(final String locator, final int timeOutSeconds) throws IOException {

		logger.startStep("skip(" + escape("waitForVisible(" + escape(locator) + ", " + timeOutSeconds + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public SwitchTo switchTo() throws IOException {

		logger.startStep("skip(" + escape("switchTo()") + ")");

		logger.endStep();

		return new SwitchToNullImpl(underlyingCommand);
	}

	@Override
	public Command log(@Nullable final String text) throws IOException {

		logger.startStep("skip(" + escape("log(" + escape(text) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command clear(final String locator) throws IOException {

		logger.startStep("skip(" + escape("clear(" + escape(locator) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command click(final String locator) throws IOException {

		logger.startStep("skip(" + escape("click(" + escape(locator) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command submit(final String locator) throws IOException {

		logger.startStep("skip(" + escape("submit(" + escape(locator) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command select(final String locator, final int value) throws IOException {

		logger.startStep("skip(" + escape("select(" + escape(locator) + ", " + value + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command select(final String locator, final String value) throws IOException {

		logger.startStep("skip(" + escape("select(" + escape(locator) + ", " + escape(value) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command assertHasClass(final String locator, final String className) throws IOException {

		logger.startStep("skip(" + escape("assertHasClass(" + escape(locator) + ", " + escape(className) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command assertDoesntHaveClass(final String locator, final String className) throws IOException {

		logger.startStep(
				"skip(" + escape("assertDoesntHaveClass(" + escape(locator) + ", " + escape(className) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command sendKeys(final String locator, final CharSequence... keys) throws IOException {

		logger.startStep("skip(" + escape("sendKeys(" + escape(locator) + "" + formatKeys(keys) + ")") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command sendKeysSecret(final String locator, final String secret) throws IOException {

		logger.startStep("skip(" + escape("sendKeysSecret(" + escape(locator) + ", ***)") + ")");

		logger.endStep();

		return underlyingCommand;
	}

	@Override
	public Command when(final String label, final boolean condition) throws IOException {

		throw new IllegalStateException("Illegal syntax: skip().when()");
	}

	@Override
	public Command when(final boolean condition) throws IOException {

		throw new IllegalStateException("Illegal syntax: skip().when()");
	}

	@Override
	public Command skip() throws IOException {

		throw new IllegalStateException("Illegal syntax: skip().skip()");
	}
}
