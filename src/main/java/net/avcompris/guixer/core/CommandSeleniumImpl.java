package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.guixer.core.Validations.assertValidLabel;
import static org.apache.commons.lang3.StringUtils.normalizeSpace;
import static org.apache.commons.lang3.StringUtils.split;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nullable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

final class CommandSeleniumImpl implements Command {

	private final WebDriver driver;
	private final String baseURL;
	private final Context context;

	public CommandSeleniumImpl( //
			final WebDriver driver, //
			final String baseURL, //
			final Context context) {

		this.driver = checkNotNull(driver, "driver");
		this.baseURL = checkNotNull(baseURL, "baseURL");
		this.context = checkNotNull(context, "context");
	}

	@Override
	public Command waitFor(final String locator) {

		return waitFor(locator, 10);
	}

	@Override
	public Command waitFor(final String locator, final int timeOutSeconds) {

		checkNotNull(locator, "locator");

		final WebDriverWait wait = new WebDriverWait(driver, timeOutSeconds);

		wait.until(presenceOfElementLocated(LocatorUtils.by(locator)));

		return this;
	}

	@Override
	public Command waitForVisible(final String locator) {

		return waitForVisible(locator, 10);
	}

	@Override
	public Command waitForVisible(final String locator, final int timeOutSeconds) {

		checkNotNull(locator, "locator");

		final WebDriverWait wait = new WebDriverWait(driver, timeOutSeconds);

		wait.until(visibilityOfElementLocated(LocatorUtils.by(locator)));

		return this;
	}

	@Override
	public Command sleep(final int ms) {

		try {

			Thread.sleep(ms);

		} catch (final InterruptedException e) {

			throw new RuntimeException(e);
		}

		return this;
	}

	@Override
	public Command takeScreenshot() throws IOException {

		return takeScreenshotWithSuffix(".png");
	}

	private Command takeScreenshotWithSuffix(final String suffix) throws IOException {

		context.incStepCount();

		final File file;

		try {

			file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		} catch (final WebDriverException e) {

			e.printStackTrace();

			throw e;
		}

		final File testDir = context.getSubDir();

		FileUtils.copyFile(file, new File(testDir, context.formatStepCount() + suffix));

		return this;
	}

	@Override
	public Command takeScreenshot(final String label) throws IOException {

		assertValidLabel(label);

		return takeScreenshotWithSuffix("-" + label + ".png");
	}

	@Override
	public Command get() {

		driver.get(baseURL);

		return this;
	}

	@Override
	public SwitchTo switchTo() {

		return new SwitchToSeleniumImpl( //
				driver.switchTo(), //
				this);
	}

	@Override
	public Command log(@Nullable final String text) {

		// do nothing

		return this;
	}

	private WebElement findElement(final String locator) {

		return driver.findElement(LocatorUtils.by(locator));
	}

	@Override
	public Command clear(final String locator) {

		findElement(locator).clear();

		return this;
	}

	@Override
	public Command click(final String locator) {

		findElement(locator).click();

		return this;
	}

	@Override
	public Command submit(final String locator) {

		findElement(locator).submit();

		return this;
	}

	@Override
	public Command assertHasClass(final String locator, final String className) {

		return wrapAssertion(locator, className, () -> {

			final String classNames = findElement(locator).getAttribute("class");

			final String normalizedClassNames = normalizeSpace(classNames);

			assertTrue(ArrayUtils.contains(split(normalizedClassNames, " "), className),
					"Classes should include: " + className + ", but was: " + normalizedClassNames);
		});
	}

	@Override
	public Command assertDoesntHaveClass(final String locator, final String className) {

		return wrapAssertion(locator, className, () -> {

			final String classNames = findElement(locator).getAttribute("class");

			final String normalizedClassNames = normalizeSpace(classNames);

			assertFalse(ArrayUtils.contains(split(normalizedClassNames, " "), className),
					"Classes should not include: " + className + ", but was: " + normalizedClassNames);
		});
	}

	private Command wrapAssertion(final String arg0, final String arg1, final Runnable runnable) {

		final String assertionName = extractAssertMethodName();

		final Assertion assertion = new Assertion(assertionName, arg0, arg1);

		context.addAssertion(assertion);

		try {

			runnable.run();

			assertion.setSuccess();

		} catch (final RuntimeException | Error e) {

			e.printStackTrace();

			assertion.setException(e);
		}

		return this;
	}

	private static String extractAssertMethodName() {

		for (final StackTraceElement ste : Thread.currentThread().getStackTrace()) {

			final String methodName = ste.getMethodName();

			if (methodName.startsWith("assert")) {

				return methodName;
			}
		}

		throw new IllegalStateException("Cannot extract assertMethodName");
	}

	@Override
	public Command sendKeys(final String locator, final CharSequence... keys) {

		findElement(locator).sendKeys(keys);

		return this;
	}

	@Override
	public Command sendKeysSecret(final String locator, final String secret) {

		findElement(locator).sendKeys(secret);

		return this;
	}

	@Override
	public Command select(final String location, final int value) throws IOException {

		return select(location, Integer.toString(value));
	}

	@Override
	public Command select(final String location, final String value) throws IOException {

		final Select select = new Select(findElement(location));

		select.selectByValue(value);

		return this;
	}

	@Override
	public Command skip() throws IOException {

		throw new IllegalStateException();
	}

	@Override
	public Command when(final String label, final boolean condition) throws IOException {

		throw new IllegalStateException();
	}

	@Override
	public Command when(final boolean condition) throws IOException {

		throw new IllegalStateException();
	}
}
