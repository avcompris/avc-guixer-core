package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.guixer.core.Validations.assertValidTestClassSimpleName;
import static net.avcompris.guixer.core.Validations.assertValidTestMethodName;
import static org.apache.commons.lang3.StringUtils.join;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

final class Context {

	private String seleniumServerURL;
	private String seleniumDesiredCapabilities;
	private String baseURL;
	private final File baseDir;
	private String contextName = "undefined";
	private Logger dumperLogger = null;
	private Logger consoleLogger = null;
	private int stepCount;
	private final List<Assertion> assertions = new ArrayList<>();

	Context(final File baseDir) {

		this.baseDir = checkNotNull(baseDir, "baseDir");
	}

	public String getSeleniumServerURL() {

		final String seleniumServerURLCopy = seleniumServerURL;

		if (seleniumServerURLCopy == null) {

			throw new IllegalStateException("seleniumServerURL should not be null");
		}

		return seleniumServerURLCopy;
	}

	public String getSeleniumDesiredCapabilities() {

		final String seleniumDesiredCapabilitiesCopy = seleniumDesiredCapabilities;

		if (seleniumDesiredCapabilitiesCopy == null) {

			throw new IllegalStateException("seleniumDesiredCapabilities should not be null");
		}

		return seleniumDesiredCapabilitiesCopy;
	}

	public String getBaseURL() {

		final String baseURLCopy = baseURL;

		if (baseURLCopy == null) {

			throw new IllegalStateException("baseURL should not be null");
		}

		return baseURLCopy;
	}

	public Logger getDumperLogger() throws IOException {

		final Logger oldLogger = dumperLogger;

		final Logger newLogger = DumperLogger.getLogger(this);

		if (oldLogger == null) {

			final TestContext testContext = extractTestContext();

			newLogger.setTestContext(testContext.testClass, testContext.testMethod);

		} else if (oldLogger.equals(newLogger)) {

			// do nothing

		} else {

			throw new IllegalStateException("oldLogger: " + oldLogger + ", newLogger: " + newLogger);
		}

		this.dumperLogger = newLogger;

		return dumperLogger;
	}

	public Logger getConsoleLogger() throws IOException {

		final Logger oldLogger = consoleLogger;

		final Logger newLogger = ConsoleLogger.getLogger(this);

		if (oldLogger == null) {

			final TestContext testContext = extractTestContext();

			newLogger.setTestContext(testContext.testClass, testContext.testMethod);

		} else if (oldLogger.equals(newLogger)) {

			// do nothing

		} else {

			throw new IllegalStateException("oldLogger: " + oldLogger + ", newLogger: " + newLogger);
		}

		this.consoleLogger = newLogger;

		return consoleLogger;
	}

	public void setSeleniumServerURL(final String seleniumServerURL) {

		this.seleniumServerURL = checkNotNull(seleniumServerURL, "seleniumServerURL");
	}

	public void setSeleniumDesiredCapabilities(final String seleniumDesiredCapabilities) {

		this.seleniumDesiredCapabilities = checkNotNull(seleniumDesiredCapabilities, "seleniumDesiredCapabilities");
	}

	public void setBaseURL(final String baseURL) {

		this.baseURL = checkNotNull(baseURL, "baseURL");
	}

	Context reset(final String contextName) {

		this.contextName = checkNotNull(contextName, "contextName");

		stepCount = 0;

		return this;
	}

	int incStepCount() {

		++stepCount;

		return stepCount;
	}

	int getStepCount() {

		return stepCount;
	}

	String formatStepCount() {

		return String.format("%04d", stepCount);
	}

	File getSubDir() throws IOException {

		final TestContext testContext = extractTestContext();

		final Class<?> testClass = testContext.testClass;
		final Method testMethod = testContext.testMethod;

		final String testClassSimpleName = testClass.getSimpleName();
		final String testMethodName = testMethod.getName();

		assertValidTestClassSimpleName(testClassSimpleName);
		assertValidTestMethodName(testMethodName);

		final File subDir = new File(baseDir,

				testClassSimpleName + "/" + testMethodName + "/" + contextName);

		FileUtils.forceMkdir(subDir);

		return subDir;
	}

	private static final class TestContext {

		public final Class<?> testClass;
		public final Method testMethod;

		public TestContext(final Class<?> testClass, final Method testMethod) {

			this.testClass = checkNotNull(testClass, "testClass");
			this.testMethod = checkNotNull(testMethod, "testMethod");
		}

		@Override
		public int hashCode() {

			return testClass.hashCode() //
					+ testMethod.hashCode();
		}

		@Override
		public boolean equals(@Nullable final Object arg) {

			if (arg == null || !(arg instanceof TestContext)) {
				return false;
			}

			final TestContext testContext = (TestContext) arg;

			return testClass.equals(testContext.testClass) //
					&& testMethod.equals(testContext.testMethod);
		}
	}

	private static TestContext extractTestContext() {

		TestContext testContext = null;

		for (final StackTraceElement ste : Thread.currentThread().getStackTrace()) {

			final String className = ste.getClassName();
			final String methodName = ste.getMethodName();

			final Class<?> clazz;

			try {

				clazz = Class.forName(className);

			} catch (final ClassNotFoundException e) {

				continue;
			}

			final Method method;

			try {

				method = clazz.getMethod(methodName);

			} catch (final NoSuchMethodException e) {

				continue;
			}

			if (!method.isAnnotationPresent(Test.class)) {

				continue;
			}

			testContext = new TestContext(clazz, method);
		}

		if (testContext != null) {

			return testContext;
		}

		throw new IllegalStateException();
	}

//	private static Method getTestMethod() {
//
//		return extractTestContext().testMethod;
//	}

	public void addAssertion(final Assertion assertion) {

		checkNotNull(assertion, "assertion");

		assertions.add(assertion);
	}

	public void close() throws IOException {

		if (dumperLogger != null) {

			dumperLogger.close();

			dumperLogger = null;
		}

		consoleLogger = null;

		final List<Assertion> failedAssertions = new ArrayList<>();

		for (final Assertion assertion : assertions) {

			if (!assertion.isSuccess()) {

				if (failedAssertions.isEmpty()) {

					System.err.println("There are some failed assertions:");
				}

				failedAssertions.add(assertion);

				System.err.println("  - " + assertion);
			}
		}

		if (!failedAssertions.isEmpty()) {

			fail("There are some failed assertions: " + join(assertions, ", "));
		}
	}
}
