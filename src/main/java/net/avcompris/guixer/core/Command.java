package net.avcompris.guixer.core;

import java.io.IOException;

public interface Command {

	Command get() throws IOException;

	Command takeScreenshot() throws IOException;

	Command takeScreenshot(String label) throws IOException;

	Command sleep(int ms) throws IOException;

	Command waitFor(String locator) throws IOException;

	Command waitFor(String locator, int timeOutSeconds) throws IOException;

	Command waitForVisible(String locator) throws IOException;

	Command waitForVisible(String locator, int timeOutSeconds) throws IOException;

	SwitchTo switchTo() throws IOException;

	Command log(String text) throws IOException;

	Command clear(String locator) throws IOException;

	Command click(String locator) throws IOException;

	Command sendKeys(String locator, CharSequence... keys) throws IOException;

	Command sendKeysSecret(String locator, String secret) throws IOException;

	Command submit(String locator) throws IOException;

	Command select(String location, int value) throws IOException;

	Command select(String location, String value) throws IOException;

	Command assertHasClass(String locator, String className) throws IOException;

	Command assertDoesntHaveClass(String locator, String className) throws IOException;

	Command skip() throws IOException;

	Command when(String label, boolean condition) throws IOException;

	Command when(boolean condition) throws IOException;
}
