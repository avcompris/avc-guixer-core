package net.avcompris.guixer.core;

import java.io.IOException;

import javax.annotation.Nullable;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;

final class CommandDumperLoggerImpl extends AbstractCommandLoggerImpl {

	public CommandDumperLoggerImpl( //
			final Command delegate, //
			final Context context, //
			@Nullable final String actionShortDescription //
	) throws IOException {

		super(delegate, context, context.getDumperLogger(), actionShortDescription);
	}

	public CommandDumperLoggerImpl( //
			final Command delegate, //
			final Context context //
	) throws IOException {

		this(delegate, context, null);
	}

	@Override
	protected void handleError(final WebDriverException e) throws IOException {

		logger.error(e);

		final String label;

		if (e instanceof TimeoutException) {

			label = "TIMEOUT_ERROR";

		} else if (e instanceof InvalidSelectorException) {

			label = "INVALID_SELECTOR_ERROR";

		} else if (e instanceof NoSuchElementException) {

			label = "NO_SUCH_ELEMENT_ERROR";

		} else if (e instanceof ElementNotVisibleException) {

			label = "ELEMENT_NOT_VISBILE_ERROR";

		} else if (e instanceof ElementNotInteractableException) {

			label = "ELEMENT_NOT_INTERACTABLE_ERROR";

		} else {

			label = "ERROR";
		}

		takeScreenshot(label);

		throw e;
	}
}
