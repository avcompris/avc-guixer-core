package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;

abstract class Validations {

	public static void assertValidTestClassSimpleName(final String classSimpleName) {

		checkNotNull(classSimpleName, "classSimpleName");

		final String regex = "[A-Z][\\w]+Test";

		if (!classSimpleName.matches(regex)) {

			throw new AssertionError(
					"classSimpleName should match \"" + regex + "\", but was: \"" + classSimpleName + "\"");
		}
	}

	public static void assertValidTestMethodName(final String methodName) {

		checkNotNull(methodName, "methodName");

		final String regex = "[a-z][\\w]+";

		if (!methodName.matches(regex)) {

			throw new AssertionError("methodName should match \"" + regex + "\", but was: \"" + methodName + "\"");
		}
	}

	public static void assertValidLabel(final String label) {

		checkNotNull(label, "label");

		final String regex = "[\\w]+([\\.-][\\w]+)*";

		if (!label.matches(regex)) {

			throw new AssertionError("label should match \"" + regex + "\", but was: \"" + label + "\"");
		}
	}
}
