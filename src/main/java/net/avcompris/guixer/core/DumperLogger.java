package net.avcompris.guixer.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.io.Resources.getResource;
import static com.google.common.io.Resources.toByteArray;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.joda.time.DateTimeZone.UTC;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.domdumper.Dumper;
import net.avcompris.domdumper.XMLDumpers;

final class DumperLogger implements Logger {

	private static String GUIXER_VERSION;

	private static final Map<File, DumperLogger> LOGGERS = newHashMap();

	private final Context context;
	private final File logXmlFile;
	private final Dumper dumper;
	private Dumper commandDumper;
	private Dumper stepDumper;
	private long currentStartedAtMs;

	private DumperLogger(final Context context, final File logXmlFile) throws IOException {

		this.context = checkNotNull(context, "context");
		this.logXmlFile = checkNotNull(logXmlFile, "logXmlFile");

		final DateTime now = new DateTime().withZone(UTC);

		dumper = XMLDumpers.newDumper("test", logXmlFile, UTF_8) //
				.addAttribute("guixerVersion", getGuixerVersion()) //
				.addAttribute("startedAt", now.toString()) //
				.addAttribute("startedAtMs", Long.toString(now.getMillis())) //
				.addAttribute("seleniumServerURL", context.getSeleniumServerURL()) //
				.addAttribute("seleniumDesiredCapabilities", context.getSeleniumDesiredCapabilities()) //
				.addAttribute("baseURL", context.getBaseURL());
	}

	@Override
	public void setTestContext(final Class<?> testClass, final Method testMethod) throws IOException {

		checkNotNull(testClass, "testClass");
		checkNotNull(testMethod, "testMethod");

		dumper //
				.addAttribute("testClassName", testClass.getName()) //
				.addAttribute("testClassSimpleName", testClass.getSimpleName()) //
				.addAttribute("testMethodName", testMethod.getName());
	}

	@Override
	public int hashCode() {

		return logXmlFile.hashCode();
	}

	@Override
	public boolean equals(@Nullable final Object arg) {

		if (arg == null || !(arg instanceof DumperLogger)) {

			return false;
		}

		return logXmlFile.equals(((DumperLogger) arg).logXmlFile);
	}

	@Override
	public String toString() {

		return "[" + logXmlFile.getAbsolutePath() + "]";
	}

	public static DumperLogger getLogger(final Context context) throws IOException {

		checkNotNull(context, "context");

		final File dir = context.getSubDir();

		if (!dir.isDirectory()) {

			throw new FileNotFoundException("dir should be a directory: " + dir.getCanonicalPath());
		}

		final File logXmlFile = new File(dir, "log.xml");

		if (logXmlFile.isFile() && LOGGERS.containsKey(logXmlFile)) {

			return LOGGERS.get(logXmlFile);
		}

		final DumperLogger logger = new DumperLogger(context, logXmlFile);

		LOGGERS.put(logXmlFile, logger);

		return logger;
	}

	public void close() throws IOException {

		commandDumper = null;

		dumper.close();

		LOGGERS.remove(logXmlFile);
	}

	public void startCommand(@Nullable final String actionShortDescription) throws IOException {

		commandDumper = dumper.addElement("command");

		if (actionShortDescription != null) {

			commandDumper.addAttribute("actionShortDescription", actionShortDescription);
		}
	}

	public void startStep(final String label) throws IOException {

		checkNotNull(label, "label");

		checkState(commandDumper != null, //
				"commandDumper should not be null; startCommand() should have been called prior to this call");

		stepDumper = commandDumper.addElement("step");

		currentStartedAtMs = System.currentTimeMillis();

		stepDumper.addElement("startedAtMs") //
				.addCharacters(Long.toString(currentStartedAtMs));

		stepDumper.addElement("literal").addCharacters(label);
	}

	public void endStep() throws IOException {

		checkState(stepDumper != null,
				"stepDumper should not be null; startStep() should have been called prior to this call");

		stepDumper.addElement("index") //
				.addCharacters(Integer.toString(context.getStepCount()));

		final long endedAtMs = System.currentTimeMillis();
		final long elapsedMs = endedAtMs - currentStartedAtMs;

		stepDumper.addElement("endedAtMs") //
				.addCharacters(Long.toString(endedAtMs));

		stepDumper.addElement("elapsedMs") //
				.addCharacters(Long.toString(elapsedMs));

		stepDumper = null;
	}

	public void error(final Throwable throwable) throws IOException {

		checkNotNull(throwable, "throwable");

		checkState(stepDumper != null,
				"stepDumper should not be null; startStep() should have been called prior to this call");

		stepDumper.addElement("error") //
				.addCharacters(throwable.toString());

		final long endedAtMs = System.currentTimeMillis();
		final long elapsedMs = endedAtMs - currentStartedAtMs;

		stepDumper.addElement("endedAtMs") //
				.addCharacters(Long.toString(endedAtMs));

		stepDumper.addElement("elapsedMs") //
				.addCharacters(Long.toString(elapsedMs));

		stepDumper = null;
	}

	private static String getGuixerVersion() throws IOException {

		if (GUIXER_VERSION != null) {

			return GUIXER_VERSION;
		}

		final Properties properties = new Properties();

		final String resourceName = "appinfo.properties";

		properties.load(new ByteArrayInputStream(toByteArray(getResource(resourceName))));

		GUIXER_VERSION = properties.getProperty("guixerVersion");

		if (isBlank(GUIXER_VERSION)) {
			throw new IOException("Cannot read GUIXER_VERSION from: " + resourceName);
		}

		return GUIXER_VERSION;
	}
}
